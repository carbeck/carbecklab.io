#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Carsten Becker'
SITENAME = 'Carsten Becker'
# SITESUBTITLE = ''
SITEURL = ''
DEFAULT_METADATA = {'author': 'carsten',}

# Path settings
PATH = 'content'
STATIC_PATHS = ["static", "downloads",]
PLUGIN_PATHS = ["plugins",]
THEME_TEMPLATES_OVERRIDES = ["template-overrides",]
EXTRA_PATH_METADATA = {'static/images/favicon.ico': {'path': 'favicon.ico'}}
OUTPUT_PATH = "public"

# Path overrides for nicer URLs
ARTICLE_URL = 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'blog/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'

# Locales
TIMEZONE = 'Europe/Berlin'
DEFAULT_LANG = 'de'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# # Blogroll
# LINKS = (('Pelican', 'https://getpelican.com/'),
#          ('Python.org', 'https://www.python.org/'),
#          ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
#          ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (
          ('mastodon', 'https://mastodon.online/ChrPistorius'),
          ('github', 'https://github.com/carbeck'),
          ('gitlab', 'https://gitlab.com/carbeck'),
          ('pixelfed', 'https://pixelfed.de/carbeck'),
#          ('envelope','#'),
          )

DEFAULT_PAGINATION = 10
GZIP_CACHE = True

# Theme settings

THEME = 'attila' # https://github.com/arulrajnet/attila
CSS_OVERRIDE = ["static/css/user.css",]
# HEADER_COVER = 'static/images/header-sticks.jpg'
TYPOGRIFY = True
TYPOGRIFY_DASHES = 'oldschool'

SHOW_CREDITS = {
    "left": 'Carsten Becker © 2024',
    "right": '<a href="/impressum">Impressum</a> / <a href="/privacy">Datenschutz</a> / <a rel="me" href="https://mastodon.online/@chrpistorius">🐘</a>', # / <a href="/legal">Legal</a>',
    }

AUTHORS_BIO = {
  "carsten": {
    "name": "Dr. Carsten Becker",
    "cover": "static/images/header-leaves.jpg",
    "image": "static/images/carsten.jpg",
    # "website": "",
    "mastodon": {"handle": "ChrPistorius", "instance": "mastodon.online"},
#    "github": "carbeck",
#    "gitlab": "carbeck",
    "orcid": "0000-0002-6023-551X",
    "googlescholar": "Vw2vRjsAAAAJ",
    "location": "Berlin",
    "bio": "Sprache(n), Bücher, Fotografie. Macht was mit Alt- und Mittelhochdeutsch sowie mittelalterlichen Manuskripten."
  }
}

DISPLAY_PAGES_ON_MENU = False
MENUITEMS = (
    ('Curriculum Vitae', '/cv/'),
    ('Projekte', '/projekte/'),
    ('Lehre', '/lehre/'),
    ('Publikationen', '/pubs/'),
    ('Links', '/links/'),
    ('Kontakt', '/impressum/'),
)

# Plugin settings

# PLUGINS = [
#     'pelican-cite',
# ]

# # Pelican Cite
# PUBLICATIONS_SRC = 'content/static/bibliography.bib'
