title: Links
cover: static/images/header-links.jpg

Berufliches
===========

* [Akademie der Wissenschaften und der Literatur | Mainz](https://www.adwmainz.de/)
* [Handschriftencensus](https://handschriftencensus.de)
* [Humboldt-Universität zu Berlin, Lehrstuhl für Geschichte der deutschen Sprache](https://www.linguistik.hu-berlin.de/de/institut/professuren/sprachgeschichte/)
* [Philipps-Universität Marburg, Institut für deutsche Philologie des Mittelalters](https://www.uni-marburg.de/de/fb09/deutsche-philologie-des-mittelalters)

Privates
========

* [carbeck auf GitHub](https://www.github.com/carbeck)
* [carbeck auf GitLab](https://www.gitlab.com/carbeck)
* [chrpistorius auf Mastodon](https://mastodon.online/@chrpistorius)
* [carbeck auf Pixelfed](https://pixelfed.de/carbeck)
* [Benung. The Ayeri Language Resource](https://ayeri.de)
