title: Kontakt
cover: static/images/header-mail.jpg
slug: impressum

<p>
    C<span style="display:none;">pSlO:I11d@_</span>ar<span style="display:none;">YYmv@2hQNoB</span>ste<span style="display:none;">mPmeF u</span>n Be<span style="display:none;">FRT.WA</span>cke<span style="display:none;">i7QXiuj</span>r<br>
    Al<span style="display:none;">x,m-HwolL</span>t-Rei<span style="display:none;">HTIvRR3p---KvsTcL</span>nic<span style="display:none;">X1@g</span>kend<span style="display:none;">h9u</span>orf 32<span style="display:none;">E§</span><br>
    1<span style="display:none;">VGSgb@zw0</span>34<span style="display:none;">Lo@3I3</span>07 Be<span style="display:none;">GSRp3H</span>r<span style="display:none;">156@@ bEW</span>lin
</p>
<p>
    Te<span style="display:none;">0cpnR_Of0</span>lef<span style="display:none;">OlD,#2s,Sg</span>o<span style="display:none;">59Bx_Ct</span>n:
    <span style="display:none;">d</span>01<span style="display:none;">VIr!uUK</span>577 <span style="display:none;">ucys</span>272<span style="display:none;">mwLq*cfaW&D4</span>79<span style="display:none;">qtFc@S</span>21<br>
    E-M<span style="display:none;">7kD5e)xsnT</span>a<span style="display:none;">hWwaOVWNfR</span>il: <span style="display:none;">aSyt @wl1</span>ko<span style="display:none;">PQ3R_h</span>nta<span style="display:none;">Ko p</span>k<span style="display:none;">l@Ef</span>t (a<span style="display:none;">YJq[34O</span>t) ca<span style="display:none;">5v3vKU2jf1d0</span>rbe<span style="display:none;">wFznjrs</span>ck (pun<span style="display:none;">)VL(NiOj</span>k<span style="display:none;">nt(rL@46Ih</span>t) in<span style="display:none;">f008@IoAM</span>fo
</p>

---

**Headerfotos**

  * <strong>Startseite:</strong> Freiburg i. Br.: Münster (30.06.2017)
  * <strong>Curriculum Vitae:</strong> Seattle, WA, USA: Pike St/2nd Ave (21.09.2014)
  * <strong>Lehre:</strong> Berlin: Humboldt-Universität zu Berlin (20.04.2023)
  * <strong>Projekte:</strong> Marburg, Nähe Wehrshausen (15.07.2022)
  * <strong>Publikationen:</strong> St. Gallen, Schweiz: [Kantonsbibliothek, Vadianische Sammlung, VadSlg Ms. 302, Teil 1, Bl. 10v/11r](http://www.e-codices.unifr.ch/de/doubleview/vad/0302/10v) (25.06.2018)
  * <strong>Links:</strong> Marburg: Oberer Rotenberg (11.10.2021)
  * <strong>Kontakt/Impressum:</strong> Marburg (26.03.2022)
  * <strong>Datenschutz:</strong> Marburg: Hermann-Cohen-Weg (17.07.2011)
  
  Bei all diesen Bildern handelt es sich um eigene Aufnahmen.
  
