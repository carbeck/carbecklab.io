title: Publikationen
cover: static/images/header-manuscript.jpg
slug: pubs

Monographien
============

2024
:	_Genusresolution bei mittelhochdeutsch_ beide. _Eine Analyse im Rahmen der Lexical-Functional Grammar_ (Advances in Historical Linguistics 1). Berlin: Language Science Press. DOI: 10.5281/zenodo.10451456. [E-Book 🔓](https://langsci-press.org/catalog/book/431).

Aufsätze
========


im Erscheinen
:	"Genusresolution bei mhd. *beide* im Referenzkorpus Mittelhochdeutsch". In Lena Schnee, Henriette Huber und Stefan Hartmann (Hrsg.), *Historische Grammatik(en)*. Jahrbuch für germanistische Sprachgeschichte 16. Berlin: De Gruyter. <!-- [Aufsatz 🔒](https://doi.org/10.1515/jbgsg-2025-00XX).
-->

2022
:	Mit Oliver Schallert. "Urkunden als Quelle (schreib-)sprachlandschaftlicher Variation: Bestandsaufnahmen und Perspektiven". *Zeitschrift für deutsches Altertum und deutsche Literatur* 151 (2): 143–178. DOI: 10.3813/zfda-2022-0005. [Aufsatz 🔒](https://doi.org/10.3813/zfda-2022-0005).

​
:	Mit Oliver Schallert. "Areale Variation im Bairischen des 13. Jahrhunderts: Eine Innen- und Außenperspektive". In Philip C. Vergeiner, Stephan Elspaß und Dominik Wallner (Hrsg.), *Struktur von Variation zwischen Individuum und Gesellschaft: Akten der 14. Bayerisch-Österreichischen Dialektologietagung, Salzburg, 7.–9.11.2019*. Zeitschrift für Dialektologie und Linguistik Beihefte 189. S. 387–407. Stuttgart: Steiner. [DNB](https://d-nb.info/1262590809).

2021
:	Mit Oliver Schallert. "Areal variation in Middle High German: A perspective from charters". *North-Western European Language Evolution* 74 (2): 199–241. DOI: 10.1075/nowele.00057.bec. [Aufsatz 🔒](https://doi.org/10.1075/nowele.00057.bec).

2020
:	Mit Teresa R. Küppers und Lara Schwanitz. "Ein neues Fragment von 'Unser vrouwen klage'". *Maniculae* 1, 9–15. DOI: 10.21248/maniculae.4. [Aufsatz 🔓](https://doi.org/10.21248/maniculae.4).

Vorträge
========

2024
:	"Genusresolution bei mhd. *beide* im Referenzkorpus Mittelhochdeutsch". <nobr>16. Jahrestagung</nobr> der Gesellschaft für germanistische Sprachgeschichte (GGSG), Düsseldorf, 25.–27.09.2024. [Abstract im Programmheft (S. 5–6)](https://ggsg2024.phil.hhu.de/wp-content/uploads/2024/09/Programm_BoA_GGSG2024.pdf), [Folien]({static}/downloads/slides/202409-ggsg-beide_rem.pdf).

2023
:	Mit Daniel Hrbek und Oliver Schallert. "Areale Variation im Mittelhochdeutschen: Methodologische Aspekte
und neue Befunde". <nobr>15. Jahrestagung</nobr> der Gesellschaft für germanistische Sprachgeschichte (GGSG), Passau, 25.–27.09.2023. [Abstract im Programmheft (S. 5–6)](https://www.geku.uni-passau.de/fileadmin/dokumente/fakultaeten/phil/lehrstuehle/werth/GGSG_Tagung/GGSG_-_Abstract-Heft_2023_Stand_Sept23.pdf).

2022
:	Mit Oliver Schallert. "Areal variation in Middle High German: Methodological and quantitative aspects". <nobr>44. Jahrestagung</nobr> der Deutschen Gesellschaft für Sprachwissenschaft (DGfS), Tübingen (per Video), 22.–25.02.2022. [Abstract im Programmheft (S. 83)](https://uni-tuebingen.de/forschung/forschungsschwerpunkte/sonderforschungsbereiche/sfb-833/dgfs-23-25022022/dgfs-2022/programm/).

2020
:	Mit Oliver Schallert. "Areal variation in Middle High German: Methodological und quantitative aspects". Grammar and Corpora 2020, Krakau (per Video), 25.–27.11.2020. [Abstract](https://gac2020.ijp.pan.pl/GaC_2020_paper_3.pdf).

2018
:	"Zum Katalog regionaltypischer Graphien für den Handschriftencensus". Arbeitsgespräche zur historischen Lexikographie, Bullay, 27.–29.04.2018. [Abstract](https://www.uni-trier.de/fileadmin/forschung/maw/MWB/Arbeitsgespr%C3%A4ch2018/Carsten_Becker_Abstract_Bullay_2018.pdf).

Rezensionen
===========

2017
:	"Rezension über: Mads Christiansen, Von der Phonologie in die Morphologie. Diachrone Studien zur Präposition-Artikel-Enklise im Deutschen (Germanistische Linguistik Monographien 32), Hildesheim/Zürich/New York 2016". *Zeitschrift für deutsches Altertum und deutsche Literatur* 146 (1): 92–94. [Abschnitt 🔓](https://www.ingentaconnect.com/contentone/dav/zfda/2017/00000146/00000001/art00006).

Abschlussarbeiten
=================

2022
:	"Kongruenzphänomene im Mittelhochdeutschen: Gender Resolution bei mittelhochdeutsch *beide*. Eine Analyse im Rahmen der Lexical-Functional Grammar". Dissertation, Philipps-Universität Marburg.

2016
:	"Mittelhochdeutsche Adjektivmorphosyntax in pragmatischen und literarischen Texten: Eine exemplarische Studie". Masterarbeit, Philipps-Universität Marburg.

2013
:	"Die neuhochdeutsche Diphthongierung und ihre Spuren im 'Corpus der altdeutschen Originalurkunden bis zum Jahr 1300'". Bachelorarbeit, Philipps-Universität Marburg.

Hobby-Veröffentlichungen
========================

2018
:	*A grammar of Ayeri: Documenting a fictional language.* Benung. The Ayeri Language Resource. Marburg: Selbstveröffentlichung, Lulu Press. [E-Book 🔓](https://ayeri.de/grammar).
