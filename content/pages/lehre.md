title: Lehre
cover: static/images/header-chairs.jpg
slug: lehre

Universitäre Lehre
==================

HU Berlin
:	"Althochdeutsch". Seminar. Humboldt-Universität zu Berlin, Institut für deutsche Sprache und Linguistik,
	[WiSe 2022/23](https://agnes.hu-berlin.de/lupo/rds?state=verpublish&status=init&vmfile=no&publishid=196582&moduleCall=webInfo&publishConfFile=webInfo&publishSubDir=veranstaltung),
	[2023/24](https://agnes.hu-berlin.de/lupo/rds?state=verpublish&status=init&vmfile=no&publishid=210086&moduleCall=webInfo&publishConfFile=webInfo&publishSubDir=veranstaltung),
	[2024/25](https://agnes.hu-berlin.de/lupo/rds?state=verpublish&status=init&vmfile=no&publishid=224616&moduleCall=webInfo&publishConfFile=webInfo&publishSubDir=veranstaltung).

​
:	"Einführung in die historische Grammatik des Deutschen". Seminar (Grundkurs). Humboldt-Universität zu Berlin, Institut für deutsche Sprache und Linguistik,
	[SoSe 2023](https://agnes.hu-berlin.de/lupo/rds?state=verpublish&status=init&vmfile=no&publishid=203339&moduleCall=webInfo&publishConfFile=webInfo&publishSubDir=veranstaltung),
	[2024](https://agnes.hu-berlin.de/lupo/rds?state=verpublish&status=init&vmfile=no&publishid=216152&moduleCall=webInfo&publishConfFile=webInfo&publishSubDir=veranstaltung),
	[2025](https://agnes.hu-berlin.de/lupo/rds?state=verpublish&status=init&vmfile=no&publishid=228580&moduleCall=webInfo&publishConfFile=webInfo&publishSubDir=veranstaltung).

Uni Marburg
:	"Einführung in die mittelalterliche Literatur". Seminar/Mittelseminar. Philipps-Universität Marburg, Institut für deutsche Philologie des Mittelalters,
	[SoSe 2022](https://marvin.uni-marburg.de:443/qisserver/pages/startFlow.xhtml?_flowId=detailView-flow&unitId=4832&periodId=2225&navigationPosition=studiesOffered,searchCourses?showBackButton=true).

Workshops
=========

2021
:	"Kartierung ortspunktbezogener Daten mit QGIS: Eine Einführung". Ludwig-Maximilians-Universität München, Institut für deutsche Philologie, Arbeitsgruppe Schallert (per Video), 25.–26. März 2021. [Folien]({static}/downloads/slides/202103-qgis-workshop.pdf).
