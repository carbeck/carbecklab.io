title: Datenschutz
slug: privacy
cover: static/images/header-fence.jpg
status: hidden

# Cookies

Diese Website wird [statisch](https://getpelican.com) generiert und setzt keine Cookies. Auf die Einbindung von Werbung sowie Kommentar- und Trafficanalysefunktionen wurde verzichtet.

# Inhalte von Drittanbietern

Diese Webseite wird auf [GitLab](https://gitlab.com) gehostet. Das verwendete [Design](https://arulrajnet.github.io/attila/) wurde so [modifiziert](https://github.com/carbeck/attila/commit/e96e14), dass keine Inhalte (Schriften, Skripte) von Drittanbietern geladen werden.

# Auskunft und Widerspruch

Kontaktdaten entnehmen Sie bitte dem [Impressum]({filename}impressum.md).
