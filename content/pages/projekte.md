title: Projekte
cover: static/images/header-electricity.jpg

Berufliches
===========

Schreibsprachenraster für den Handschriftencensus
-------------------------------------------------

Sich durch Fachliteratur wälzen zu müssen um eine Handschrift (ein handgeschriebenes Schriftstück, nicht den charakteristischen Schriftduktus einer Person) sprachlandschaftlich zuverlässig einzuordnen, ist mühsam. Daher ist eines der Teilziele des Akademievorhabens [Handschriftencensus](https://handschriftencensus.de), einen Bestimmungsschlüssel anhand der einschlägigen Literatur vor allem zum Mittelhochdeutschen zu entwickeln, der die Einordnung eines Texts zumindest in die großen Schreibdialekträume des Deutschen anhand landschaftlich charakteristischer Schreibweisen erleichtert. Hierzu habe ich vorbereitende Arbeit beigesteuert.

<!--
Erklärvideos für *HSC 2020*
---------------------------

Der Handschriftencensus hat 2020 seine Webseite überarbeitet. In diesem Zuge wurden ein paar Erklärvideos zu neuen und aufgefrischten alten Funktionen auf dem ["X"-Account des Handschriftencensus](https://x.com/HSCensus) (🔒) publiziert, die ich produziert und eingesprochen habe:

* [Zum Verbleib der Repertorien im neuen Handschriftencensus](https://x.com/HSCensus/status/1314113301559693314) (🔒)
* [Vorstellung der neuen Suchfunktionen](https://x.com/HSCensus/status/1315562850899894272) (🔒)
* [Die Mitteilungsfunktion](https://x.com/HSCensus/status/1317012403406671873) (Text: Daniel Könitz) (🔒)
-->

Explorative Studien zum *Corpus der altdeutschen Originalurkunden*
------------------------------------------------------------------

Schon seit der Zeit meiner Bachelorarbeit habe ich mich immer wieder mit dem *Corpus der altdeutschen Originalurkunden bis zum Jahr 1300* (CAO; [Wilhelm et al. 1932–2004](http://tcdh01.uni-trier.de/cgi-bin/iCorpus/CorpusIndex.tcl)) beschäftigt. So sind in jüngster Zeit [mehrere Artikel]({filename}publications.md) in Kooperation mit [Oliver Schallert](https://orcid.org/0000-0002-3491-5087) entstanden, die in Hinsicht auf die weitergehende digitale Aufbereitung dieses Quellmaterials einige dialektgeographische und damit verbundene sprachhistorische Auswertungen präsentieren, Erfahrungen der letzten Jahre im Umgang mit dem Material resümieren sowie Vorurteile gegenüber Urkunden als Datenquelle für sprachhistorische Untersuchungen der älteren Forschung versuchen zu korrigieren.

Qualifikation
=============

Dissertation
------------

Mein Dissertationsprojekt ist im Grunde eine Weiterführung meiner Masterarbeit und in Anlehnung an das Projekt [Diachronie von Kongruenzsystemen](https://www.uni-marburg.de/de/fb09/igs/arbeitsgruppen/sprachgeschichte/forschungsprojekte/kongruenz) entstanden. Die Idee war, einen Kongruenzkontext näher zu untersuchen, der sonst weniger häufig Beachtung findet und auch weniger häufig vorkommt. Als Kongruenzeffekt hat gerade *Gender Resolution* ([Corbett 1991](https://doi.org/10.1017/CBO9781139166119)) – also die Auflösung der Genuskategorie bei gleichzeitigem Bezug auf Entitäten mit verschiedenem grammatischen Geschlecht – in den vergangenen Jahren viel Beachtung gefunden. Mittelhochdeutsch *beide/beidiu* als Quantor erscheit also einschlägig, bezieht es sich doch prototypisch auf zwei Personen oder Dinge. Als Datengrundlage dient auch für diese Untersuchung das CAO, als konventionelleres Vergleichsobjekt die deutsche 'Kaiserchronik', deren sämtliche bekannten Überlieferungsträger erst vor Kurzem digital erschlossen wurden ([Chinca, Hunter, Wolf und Young 2012—2017](http://digi.ub.uni-heidelberg.de/de/kcd/kaiserchronik.html)).

Masterarbeit
------------

In meiner Masterarbeit ("Mittelhochdeutsche Adjektivmorphosyntax in pragmatischen und literarischen Texten: Eine exemplarische Studie") habe ich mich mit der Adjektivdeklination im Mittelhochdeutschen beschäftigt. Die bis dahin gültige, großteils auf literarischen Texten basierende Standardgrammatik zum Mittelhochdeutschen ([Paul 2007](https://doi.org/10.1515/9783110942354)) macht dazu eher impressionistische als handfeste Angaben. Als Untersuchungsgegenstand dienten das CAO und  exemplarisch als literarische Vergleichsgrundlage die 'Iwein'-Handschrift B ([Gießen, Universitätsbibliothek, Hs. 97](https://nbn-resolving.de/urn:nbn:de:hebis:26-digisam-46599); [HSC](https://www.handschriftencensus.de/1102)).

Bachelorarbeit
--------------

Ziel meiner Bachelorarbeit ("Die neuhochdeutsche Diphthongierung und
ihre Spuren im 'Corpus der altdeutschen Originalurkunden bis zum Jahr 1300'") war es, das bekannte Phänomen der Entwicklung der mittelhochdeutschen Langvokale *î*, *iu* und *û* hin zu neuhochdeutsch *ei*, *eu* und *au* anhand der Urkunden des CAO nachzuvollziehen und im Grunde damit zu überprüfen, ob sich mit Hilfe des CAO die auch in diesem Fall unpräzisen Angaben in Paul (2007) validieren und präzisieren lassen.

Hobby
=====

Kunstsprache 'Ayeri'
--------------------

Sprachen interessieren mich seit meiner Jugend. Mit der Entwicklung der persönlichen Kunstsprache ["Ayeri"](https://ayeri.de) seit 2003 habe ich mir eine Möglichkeit geschaffen, mich kreativ mit Linguistik auseinanderzusetzen. In der Retrospektive mag Ayeri aufgrund früher, unbedarfter Entscheidungen nicht immer ganz sprachtypologisch plausibel oder stringent sein, jedoch geht es hier mehr um das intellektuelle Spiel und den Spaß am Lernen als um unbedingte Plausibilität. Das Projekt hat 2018 seinen bisherigen Höhepunkt und vorläufigen Abschluss mit der Veröffentlichung einer über vierhundertseitigen Referenzgrammatik gefunden. Neben grundlegenden Linguistik-Kenntnissen habe ich in Zusammenhang mit diesem Projekt auch Grundkenntnisse in HTML, LaTeX, PHP, Python und SQL vertieft oder erworben.

