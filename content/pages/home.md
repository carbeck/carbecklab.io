Title: Carsten Becker
URL:
save_as: index.html
template: home
cover: static/images/header-wall.jpg
status: hidden

Mein Name ist Carsten Becker und zur Zeit arbeite ich als Wissenschaftlicher Mitarbeiter mit Schwerpunkt mittelalterliche Sprachgeschichte des Deutschen am [Lehrstuhl für Geschichte der deutschen Sprache der Humboldt-Universität zu Berlin](https://www.linguistik.hu-berlin.de/de/institut/professuren/sprachgeschichte/).

In meiner Anfang 2024 erschienenen [Dissertation](https://langsci-press.org/catalog/book/431) habe ich Variation in der Kongruenzform bei mittelhochdeutsch *beid-* 'beide' in Hinblick auf die Sachtexte des *Corpus der altdeutschen Originalurkunden bis zum Jahr 1300* ([Wilhelm et al. 1932–2004](http://tcdh01.uni-trier.de/cgi-bin/iCorpus/CorpusIndex.tcl)) und die deutsche 'Kaiserchronik' ([Chinca, Hunter, Wolf und Young 2012–2017](https://doi.org/10.11588/edition.kcd)) untersucht.
