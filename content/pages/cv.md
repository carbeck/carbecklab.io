title: Curriculum Vitae
template: cv
cover: static/images/header-city.jpg
slug: cv

<dl>
<dt>2022–</dt>
    <dd>Humboldt-Universität zu Berlin, Institut für deutsche Sprache und Linguistik, Lehrstuhl für Geschichte der deutschen Sprache: Wissenschaftlicher Mitarbeiter</dd>
<dt>2022</dt>
	<dd><strong>Doktor der Philosophie in Deutscher Sprache</strong>, Philipps-Universität Marburg</dd>
<dt>2017–22</dt>
	<dd>Philipps-Universität Marburg, Institut für deutsche Philologie des Mittelalters: Wissenschaftlicher Mitarbeiter (Akademievorhaben Handschriftencensus)</dd>
<dt>2016–17</dt>
	<dd>Philipps-Universität Marburg, Institut für germanistische Sprachwissenschaft: Wissenschaftliche Hilfskraft (Prof. Dr. Jürg Fleischer)</dd>
<dt>2016</dt> 
	<dd><strong>Master of Arts in Deutscher Literatur,</strong> Philipps-Universität Marburg</dd>
<dt>2014</dt>
	<dd>Auslandssemester an der University of Victoria, British Columbia, Kanada (Fächer: Germanic Studies, English)</dd>
<dt>2013</dt> 
	<dd><strong>Bachelor of Arts in Europäischen Literaturen (Fachrichtung: Deutsch, Englisch),</strong> Philipps-Universität Marburg</dd>
<dt>2012–16</dt>
	<dd>Philipps-Universität Marburg, Institut für deutsche Philologie des Mittelalters: Studentische Hilfskraft (Prof. Dr. Jürgen Wolf)</dd>
<dt>2009</dt>
	<dd><strong>Berufsabschluss als Medienkaufmann Digital und Print,</strong> Georg Westermann Verlag, Druckerei und kartographische Anstalt GmbH & Co. KG, Braunschweig</dd>
</dl>

Stipendien
==========

<dl>
<dt>2014</dt>
	<dd>PROMOS-Stipendium des DAAD für einen Auslandsaufenthalt</dd>
</dl>


Praktika
========

<dl>
<dt>2012</dt>
	<dd>Verlag der Francke-Buchhandlung, Marburg: Lektor (Studienpraktikum, 8 Wochen)</dd>
<dt>2004</dt>
	<dd>Brunnen-Verlag, Gießen: Verlagsbuchhändler (Schulpraktikum, 2 Wochen)</dd>
</dl>

Kenntnisse
==========

* Englisch (C1); Französisch (B1)
* Mittelhochdeutsch, Althochdeutsch
* CSS, Git, HTML, LaTeX, Python, SQL, PHP
